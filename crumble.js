
//CODE GENERATION

function display_crumb(crumb) {
	return crumb.name + (crumb.value ? ": " + crumb.value : "")
}

/*
Push <value, List(any)>: adds the values to the value stack. Any functions must be of type (value, add_crumbs) => value
BuiltinCrumb <f, (any => any)>: runs a builtin function on the topmost value of the stack
Eval: evaluates the value stack. Evaluation is done by iteratively popping values and applying them to the previous value
Assign <names, List(str)>: assigns the topmost value on the value stack to the given name

Enter: pushes a fresh new empty scope on the scope stack
Leave: pops the topmost scope from the scope stack
Return: pops the topmost scope from the scope stack and pushes it onto the value stack
Delete <names, List(str)>: remove a value from the scope. This is only used to remove the parameters for functions and constructors
*/

const PushCrumb = (values) => ({ name: "Push", value: values }) //TODO change to PushEvalCrumb
const BuiltinCrumb = (f) => ({ name: "Builtin", value: f })
const EvalCrumb = { name: "Eval" }
const AssignCrumb = (names) => ({ name: "Assign", value: names })

const EnterScopeCrumb = { name: "Enter" }
const LeaveScopeCrumb = { name: "Leave" }
const ReturnScopeCrumb = { name: "Return" }
const DeleteCrumb = (names) => ({ name: "Delete", value: names })

const AppendCrumb = { name: "Append" }

//root: Node, output: {crumbs: [Crumb], errors: [Error]}
function crumble(root) {
	var errors = []
	return { crumbs: add_crumbs(root, [], errors), errors: errors }
}

function valueify(token) {
	switch (token.type) {
		case "Word":
			return token.string
		case "Ordinal":
			return new CookieanOrdinal(token.string.slice(1) - 0)
		case "Integral":
			return new CookieanIntegral(token.string - 0)
		case "DotSequence":
			return token.string // TODO
		case "ColonSequence":
			return token.string // TODO
		case "StringLiteral":
			return new CookieanText(token.string)
	}
	return token.string
}

function nameify_tokens(tokens) {
	return tokens.map((tok) => tok.string)
}

function param_crumbs(tokens, crumbs) {
	crumbs.push(AssignCrumb(nameify_tokens(tokens)))
}

function delete_param(tokens, crumbs) {
	crumbs.push(DeleteCrumb(nameify_tokens(tokens)))
}

function expr_crumbs(tokens, crumbs) {
	if (tokens.length > 0) {
		crumbs.push(PushCrumb(tokens.map(valueify)))
		crumbs.push(EvalCrumb)
		crumbs.push(AppendCrumb)
	}
}

function add_crumbs(node, crumbs, errors) {
	switch (node.name) {
		case "Apply":
			crumbs.push(PushCrumb(node.row.subject.map(valueify)))
			crumbs.push(EvalCrumb)
			crumbs.push(EnterScopeCrumb) //TODO lessen use of scopes
			expr_crumbs(node.row.object, crumbs)
			node.children.forEach((child) => add_crumbs(child, crumbs, errors))
			crumbs.push(ReturnScopeCrumb)

			crumbs.push(AssignCrumb(nameify_tokens(node.row.subject)))
			break
		case "Constructor":
			var c_crumbs = []
			c_crumbs.push(EnterScopeCrumb)
			param_crumbs(node.row.parameter, c_crumbs)
			expr_crumbs(node.row.object, c_crumbs)
			node.children.forEach((child) => add_crumbs(child, c_crumbs, errors))
			delete_param(node.row.parameter, c_crumbs)
			c_crumbs.push(ReturnScopeCrumb)

			var c_val = (add_instructions) => add_instructions(c_crumbs)
			crumbs.push(PushCrumb([c_val]))
			crumbs.push(AssignCrumb(nameify_tokens(node.row.subject)))
			break
		case "Procedure":
			var p_crumbs = []
			p_crumbs.push(EnterScopeCrumb)
			expr_crumbs(node.row.object, p_crumbs)
			node.children.forEach((child) => add_crumbs(child, p_crumbs, errors))
			p_crumbs.push(LeaveScopeCrumb)

			var p_val = (add_instructions) => add_instructions(p_crumbs)
			crumbs.push(PushCrumb([p_val]))
			crumbs.push(AssignCrumb(nameify_tokens(node.row.subject)))
			break
		case "Function":
			var f_crumbs = []
			f_crumbs.push(EnterScopeCrumb)
			param_crumbs(node.row.parameter, f_crumbs)
			expr_crumbs(node.row.object, f_crumbs)
			node.children.forEach((child) => add_crumbs(child, f_crumbs, errors))
			delete_param(node.row.parameter, f_crumbs)
			f_crumbs.push(LeaveScopeCrumb)
			
			var f_val = (add_instructions) => add_instructions(f_crumbs)
			crumbs.push(PushCrumb([f_val]))
			crumbs.push(AssignCrumb(nameify_tokens(node.row.subject)))
			break
		case "Expression":
			expr_crumbs(node.row.subject, crumbs)
			break
		case "Execute":
			crumbs.push(EnterScopeCrumb)
			expr_crumbs(node.row.object, crumbs)
			node.children.forEach((child) => add_crumbs(child, crumbs, errors))
			crumbs.push(LeaveScopeCrumb)
			break
	}
	return crumbs
}

