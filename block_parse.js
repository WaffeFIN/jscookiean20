
//(DYNAMIC) BLOCK PARSING

function display_node(node, depth) {
	depth = depth || 0
	var result = "|\t".repeat(depth) + node.name + "\n";
	if (node.children)
		node.children.forEach((child) => result = result + display_node(child, depth + 1))
	return result
}

/*
//Statements
Apply         (n is 2)
              (n is squared)
Constructor   ([color] car is ...)
Procedure     (exit does ...)
Function      ([n] squared does ...)
Context       (a slow context defines ...)
Execute       (do ...)
Possession    (Waffe has ...)
Label         ("example label": ...)

//Expressions
Generic       (2 displayed)
Cast          (... as a number ...)
Inject        (... using a slow context ...)
Restriction   (pi as 355 / 113)
              (parents as a person pair)
*/

//Node := (left, right, children, assignChild(Node))
function StatementFactory(name, has_param, has_subject, verb) {
	return () => {
		return {
			name: name,
			children: [], //TODO assign block_object as child Expression
			check_match: (row) => ((row.parameter.length > 0) == has_param) &&
				                  ((row.subject.length > 0) == has_subject) &&
				                  (row.verbs.length === 1) &&
				                  (row.verbs[0].type === verb)
		}
	}
}

const ApplyStatement       = StatementFactory("Apply", false, true, "Is")
const ConstructorStatement = StatementFactory("Constructor", true, true, "Is")
const ProcedureStatement   = StatementFactory("Procedure", false, true, "Does")
const FunctionStatement    = StatementFactory("Function", true, true, "Does")
const ContextStatement     = StatementFactory("Context", false, true, "Defines")
const ExecuteStatement     = StatementFactory("Execute", false, false, "Do")
const PossessionStatement  = StatementFactory("Possession", false, true, "Has")

const Label = () => {
	return {
		name: "Label",
		children: [],
		check_match: (row) => (!row.parameter) &&
                             (row.subject.length === 1) &&
                             (row.subject[0].type = "StringLiteral") &&
                             (row.verbs.length === 0) &&
                             (row.block_starter)
	}
}

const Expression = () => {
	return {
		name: "Expression",
		check_match: (row) => (!row.parameter.length) &&
				              (row.subject.length > 0) &&
				              (!row.verbs.length) &&
				              (!row.object.length) &&
				              (!row.block_starter) &&
				              (!row.block_object.length)
	}
}

//rows: [Row], output: {root: Node, error: [Error]}
function block_parse(rows) {
	
	var root = ExecuteStatement()
	root.row = { object: [], block_starter: true }
	var nodes = [root]
	var errors = []
	
	var node_matchers = [
		ApplyStatement,
		ConstructorStatement,
		ProcedureStatement,
		FunctionStatement,
		ContextStatement,
		ExecuteStatement,
		PossessionStatement,
		Label,
		Expression
	]
	
	function nodeify(row) {
		var matchers = [...node_matchers].map( (matcher_f) => matcher_f() )
		var matches = matchers.filter( (matcher_obj) => matcher_obj.check_match(row) )
		if (matches.length == 0) {
			errors.push(Error("Parsing", "Malformed statement", row.row))
			return { name: "Malformed", row: row }
		}
		if (matches.length > 1) { //TODO if block is started and there is no block_object the indentation must grow!
			errors.push(Error("Parsing", "Ambiguous statement. Could be any of: " + matches.map((match) => match.name), row.row))
		}
		var match = matches[0]
		match.row = row
		return match
	}
	
	function input(row) {
		if (row.indentation >= nodes.length) {
			errors.push(Error("Parsing", "Too much indentation!", row.row))
			row.indentation = nodes.length - 1
		}
		var current_node = nodeify(row)
		var parent_node = nodes[row.indentation]
		parent_node.children.push(current_node)
		
		if (row.indentation == nodes.length - 1 && !parent_node.row.block_starter)
			errors.push(Error("Parsing", "Block started unexpectedly. Colon (:) missing?", row.row))
		//check block types compatibility
		
		nodes = nodes.slice(0, row.indentation + 1)
		nodes.push(current_node)
	}
	
	rows.forEach(input)
	
	return { root: root, errors: errors }
}

