# JS-Cookiean20

This is the fourth attempt at creating Cookiean. After enough development and ironing out any kinks the Rust-based Cookiean20 repo will take over.

## Example

```
(This is a comment)

x is 10
x is displayed (outputs 10)

"Waffe" is reversed then displayed

(... equivalent to ...)

"Waffe" is:
	reversed
	displayed (outputs "effaW")

a name has:
	text as first
	text as last

a person has:
	a name
	an integer as age

Waffe is a person:
	first name is "Walter"
	last name is "Grönholm"
	age is 24

"Hello [first name of Waffe]!" displayed (outputs "Hello Walter!")
```

## Tenets

1. Unambiguous and intuitive
1. Simple
1. Minimalized yet natural English
1. Aim for context-oriented functional programming
1. Tabs rule
