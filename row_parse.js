
///ROW PARSING

function display_row(row) {
	return "\n" + row.row + "\t" +
	       "|".repeat(row.indentation) +
	       row.subject.map(display_token) +
	       row.verbs.map(display_token) +
	       row.object.map(display_token) +
	       (row.block_starter ? display_token(row.block_starter) : "") +
		   row.block_object.map(display_token)
}

const IndentationPart = {
	match_token: (token, row) => {
		if (token.type === "Tab") {
			row.indentation += 1;
			return { status: "MatchContinue" }
		} else {
			return { status: "Stop" }
		}
	}
}

function ParameterPart() {
	var parsing_contents = false;
	return {
		match_token: (token, row) => {
			if (parsing_contents) {
				switch (token.type) {
					case "BracketClose":
						parsing_contents = false;
						return { status: "MatchStop" }
					case "Word":
						row.parameter.push(token);
						return { status: "Continue" }
					default:
						return { status: "Erroneous", message: "You can only use words to name a parameter" }
				}
			} else {
				switch (token.type) {
					case "Tab":
						return { status: "Erroneous", message: "Tabs can only be placed at the start of a row" }
					case "BracketOpen":
						parsing_contents = true;
						return { status: "Continue" }
					case "BracketClose":
						return { status: "Erroneous", message: "Found a stray closed bracket" }
					default:
						return { status: "Stop" }
				}
			}
		}
	}
}

function is_verb(token) {
	return token.type === "Is" || token.type === "Defines" ||
	       token.type === "Does" || token.type === "Do" ||
	       token.type === "Has";
}

const SubjectPart = {
	match_token: (token, row) => {
		switch (token.type) {
			case "NewLine":
				return { status: "Stop" }
			case "Tab":
				return { status: "Erroneous", message: "Tabs can only be placed at the start of a row" }
			case "BracketClose":
			case "BracketOpen":
				return { status: "Erroneous", message: "Unexpected bracket. A parameter must come before any words" }
			case "Colon":
			case "QuestionMark":
				return { status: "Stop" }
			default:
				if (is_verb(token)) {
					return { status: "Stop" }
				}
				row.subject.push(token)
				return { status: "MatchContinue" }
		}
	}
}

const VerbPart = {
	match_token: (token, row) => {
		if (is_verb(token)) {
			row.verbs.push(token)
			return { status: "MatchContinue" }
		}
		return { status: "Stop" }
	}
}

const ObjectPart = {
	match_token: (token, row) => {
		switch (token.type) {
			case "NewLine":
				return { status: "Stop" }
			case "Tab":
				return { status: "Erroneous", message: "Tabs can only be placed at the start of a row" }
			case "BracketClose":
			case "BracketOpen":
				return { status: "Erroneous", message: "Unexpected bracket. A parameter must come before any words" }
			case "Colon":
			case "QuestionMark":
				return { status: "Stop" }
			default:
				if (is_verb(token)) {
					return { status: "Erroneous", message: "There must not be more than one verb part per row" }
				}
				row.object.push(token)
				return { status: "MatchContinue" }
		}
	}
}

const BlockStarterPart = {
	match_token: (token, row) => {
		switch (token.type) {
			case "Colon":
			case "QuestionMark":
				row.block_starter = token;
				return { status: "MatchStop" }
			default:
				return { status: "Stop" }
		}
	}
}

const BlockObjectPart = {
	match_token: (token, row) => {
		switch (token.type) {
			case "NewLine":
				return { status: "Stop" }
			case "Tab":
				return { status: "Erroneous", message: "Tabs can only be placed at the start of a row" }
			case "BracketClose":
			case "BracketOpen":
				return { status: "Erroneous", message: "Unexpected bracket. A parameter must come before any words" }
			case "Colon":
			case "QuestionMark":
				return { status: "Stop" }
			default:
				if (is_verb(token)) {
					return { status: "Erroneous", message: "There must not be more than one verb part per row" }
				}
				row.block_object.push(token)
				return { status: "MatchContinue" }
		}
	}
}

const NewLinePart = {
	match_token: (token, row) => {
		switch (token.type) {
			case "NewLine":
				return { status: "MatchStop" }
			default:
				return { status: "Erroneous", message: "Expected a line break, what happened here?" }
		}
	}
}

function non_empty_row(row) {
	return row.subject.length ||
	       row.verbs.length ||
	       row.object.length ||
	       row.block_object.length ||
	       row.parameter.length ||
	       row.block_starter
}

function Row(row) {
	return {
		row: row,
		indentation: 0,
		parameter: [],
		subject: [],
		verbs: [],
		object: [],
		block_starter: undefined,
		block_object: []
	};
}

//tokens: [Token], output: {rows: [Row], errors: [Error]}
function row_parse(tokens) {
	var row = 0;
	var rows = [];
	var errors = [];
	var row_parts = [
		IndentationPart,
		ParameterPart(),
		SubjectPart,
		VerbPart,
		ObjectPart,
		BlockStarterPart,
		BlockObjectPart,
		NewLinePart
	];
	var tokens_index = 0;
	var current_token = tokens[tokens_index];
	while (current_token) {
		var row_obj = Row(row);
		next_part: for (var row_parts_index = 0; row_parts_index < row_parts.length; row_parts_index += 1) {
			next_token: while (current_token) {
				var match_obj = row_parts[row_parts_index].match_token(current_token, row_obj);
				switch(match_obj.status) {
					case "MatchContinue":
					case "Continue":
						tokens_index += 1;
						current_token = tokens[tokens_index];
						continue next_token;
					case "Erroneous":
						errors.push(Error("Parsing", match_obj.message, row, current_token.column))
					case "MatchStop":
						tokens_index += 1;
						current_token = tokens[tokens_index];
					case "Stop":
						continue next_part;
				}
			}
		}
		if (non_empty_row(row_obj)) {
			rows.push(row_obj);
		}
		row += 1;
		
	}
	return { rows: rows, errors: errors };
}

