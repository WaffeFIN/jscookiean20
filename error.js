
///column is optional
function Error(type, message, row, column) {
	if (column === undefined) {
		return "@" + row + "\t" + type + ":\n" + message + "\n"
	} else {
		return "@" + row + ":" + column + "\t" + type + ":\n" + message + "\n"
	}
}
