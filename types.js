function CookieanText(s) {
	this.__value = s
}
CookieanText.prototype.__type = "Text"
CookieanText.prototype.__display = (self) => self.__value

function CookieanIntegral(n) {
	this.__value = n
}
CookieanIntegral.prototype.__type = "Integral"
CookieanIntegral.prototype.__display = (self) => self.__value

function __f(f) {
	return (self, add_crumbs) => {
		add_crumbs([BuiltinCrumb(f.bind(null, self))])
	}
}
CookieanIntegral.prototype["+"] = __f( (self, obj) => new CookieanIntegral(obj.__value + self.__value) )
CookieanIntegral.prototype["-"] = __f( (self, obj) => new CookieanIntegral(obj.__value - self.__value) )
CookieanIntegral.prototype["*"] = __f( (self, obj) => new CookieanIntegral(obj.__value * self.__value) )
CookieanIntegral.prototype["/"] = __f( (self, obj) => new CookieanIntegral(obj.__value / self.__value) )
CookieanIntegral.prototype["modulo"] = __f( (self, obj) => new CookieanIntegral(obj.__value % self.__value) )

function CookieanOrdinal(n) {
	CookieanIntegral.apply(this, [n])
}
CookieanOrdinal.prototype = Object.create(CookieanIntegral.prototype)
CookieanOrdinal.prototype.__type = "Ordinal"
CookieanOrdinal.prototype.__display = (self) => "#" + (self.__value + 1)
