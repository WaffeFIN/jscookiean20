
function to_string(val, depth) {
	depth = depth || 0
	if (depth > 5)
		return "..."
	if (val === undefined) {
		return "[Nothing]"
	}
	if (val.__display !== undefined) {
		return val.__display(val)
	}
	if (typeof val === "object") {
		var result = ""
		for (var key in val) {
			result += "\t".repeat(depth) + key + ": " + to_string(val[key], depth + 1) + "\n"
		}
		return result
	}
	return JSON.stringify(val)
}

function builtinify(library) {
	var return_obj = {}
	for (const [name, f] of Object.entries(library)) {
		return_obj[name] = (add_crumbs) => add_crumbs([BuiltinCrumb(f)])
	}
	return return_obj
}

const StandardLibrary = builtinify({
	"read": (_) => {
		return new CookieanText(user_input.length === 0 ? window.prompt("Input awaited:" + document.getElementById("output").value) : user_input.pop())
	},
	"displayed": (obj) => {
		document.getElementById("output").value += to_string(obj) + "\n"
		return obj
	},
	"incremented": (obj) => {obj.__value += 1; return obj},
	"decremented": (obj) => {obj.__value -= 1; return obj},
	"reversed": (obj) => {obj.__value = [...obj.__value].reverse().join("")}
})

var user_input = []

function AddInput() {
	var text = document.getElementById("input").value
	document.getElementById("input").value = ""
	user_input = user_input.concat(text.split("|"))
	document.getElementById("input_text").classList.remove('bold');
}