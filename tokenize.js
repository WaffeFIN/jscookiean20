
//TOKENIZING

function display_token(token) {
	return token.string + "[" + token.type + "]"
}

///type: Type, string: String
function Token(column, type, string) {
	return {
		column: column,
		type: type,
		string: string
	}
}

function SequenceMatcher(separator) {
	var expect_content = true;
	return {
		append: function(character) {
			if (character == separator) {
				if (expect_content) {
					return "Stop";
				}
				expect_content = true;
				return "Continue";
			}
			if (!is_word_letter(character)) {
				return "Stop";
			}
			expect_content = false;
			return "MatchContinue"
		},
		character_line_reset: function() { return "MatchContinue" },
		reset: function() {
			expect_content = true
		},
	}
}

function is_digit(character) {
	var str = "" + character;
	var code = str.charCodeAt(0);
    if (code >= 48 && code <= 57) // 0 to 9
    { return true; }
	return false;
}

function is_word_letter(character) {
	var str = "" + character;
	var code = str.charCodeAt(0);
    if ((code >= 35 && code <= 39) || // # to '
        (code === 42) || // *
        (code === 43) || // +
        (code === 45) || // -
        (code >= 47 && code <= 57) || // / and 0 to 9
        (code >= 60 && code <= 62) || // < to >
        (code >= 64 && code <= 90) || // @ and uppers
        (code === 92) || // \ (backslash)
        (code >= 94 && code <= 122) || // ^_` and lowers
        (code === 124) || // |
        (code === 126) || // ~
        (code >= 128 && code <= 254)) //åäö
    { return true; }
	return false;
}

const WordMatcher = {
	
	append: function(character) {
		return is_word_letter(character) ? "MatchContinue" : "Stop"
	},
	new_line_reset: function() { return "MatchContinue" },
	reset: function() {},
}

function SimpleMatcher(string) {
	
	var index = 0;
	return {
		append: function(character) {
			var return_value = character == string.charAt(index) ? (index == string.length - 1 ? "MatchStop" : "Continue") : "Stop";
			index += 1;
			return return_value;
		},
		new_line_reset: function() { return "MatchContinue" },
		reset: function() {
			index = 0;
		},
	}
}

function StringMatcher() {
	var parsing_contents = false;
	var depth = 0;
	var errorMessage = "";
	return {
		append: function(character) {
			if (character == "\"") {
				parsing_contents = !parsing_contents;
				return parsing_contents ? "Continue" : "MatchContinue";
			}
			if (character == "]" && !parsing_contents) {
				parsing_contents = true;
				if (depth > 0) {
					depth -= 1;
					return "Continue";
				} else {
					return "Stop";
				}
			}
			if (character == "[" && parsing_contents) {
				parsing_contents = false;
				depth += 1;
				return "MatchContinue";
			}
			if (character == "\n" && parsing_contents) {
				errorMessage = "The string is missing an ending quote \""
				return "Erroneous"
			}
			if (character == "[" || character == "]" || character == "\n") {
				return "Stop"
			}
			return parsing_contents ? "Continue" : "Stop";
		},
		new_line_reset: function() {
			if (depth !== 0) {
				errorMessage = "Text insertion brackets do not match. You must close them with ']\"'"
				depth = 0;
				return "Erroneous"
			}
			return "MatchContinue"
		},
		error: function() {
			return errorMessage;
		},
		reset: function() {
			parsing_contents = false;
		},
	}
}

const IntegralMatcher = {
	append: function(character) {
		return is_digit(character) ? "MatchContinue" : "Stop"
	},
	new_line_reset: function() { return "MatchContinue" },
	reset: function() {},
}

function OrdinalMatcher() {
	var expect_integral = false;
	return {
		append: function(character) {
			if (!expect_integral && character === "#") {
				expect_integral = true;
				return "MatchContinue";
			}
			return expect_integral && is_digit(character) ? "MatchContinue" : "Stop"
		},
		new_line_reset: function() { return "MatchContinue" },
		reset: function() {
			expect_integral = false;
		},
	}
}

const token_matchers = [
	{ matcher: SequenceMatcher("."), result: "DotSequence" },
	{ matcher: SequenceMatcher(":"), result: "ColonSequence" },
	{ matcher: WordMatcher, result: "Word" },
    { matcher: SimpleMatcher("is"), result: "Is" },
    { matcher: SimpleMatcher("does"), result: "Does" },
    { matcher: SimpleMatcher("do"), result: "Do" },
    { matcher: SimpleMatcher("defines"), result: "Defines" },
    { matcher: SimpleMatcher("has"), result: "Has" },
    { matcher: SimpleMatcher("as"), result: "As" },
    { matcher: SimpleMatcher("using"), result: "Using" },
    { matcher: SimpleMatcher("then"), result: "Then" },
    { matcher: SimpleMatcher(" "), result: "Space" },
    { matcher: SimpleMatcher("\t"), result: "Tab" },
    { matcher: SimpleMatcher("\n"), result: "NewLine" },
    { matcher: SimpleMatcher("("), result: "ParenthesisOpen" },
    { matcher: SimpleMatcher(")"), result: "ParenthesisClose" },
    { matcher: SimpleMatcher("["), result: "BracketOpen" },
    { matcher: SimpleMatcher("]"), result: "BracketClose" },
    { matcher: SimpleMatcher("{"), result: "BraceOpen" },
    { matcher: SimpleMatcher("}"), result: "BraceClose" },
    { matcher: SimpleMatcher("."), result: "Dot" },
    { matcher: SimpleMatcher(","), result: "Comma" },
    { matcher: SimpleMatcher(";"), result: "Semicolon" },
    { matcher: SimpleMatcher(":"), result: "Colon" },
    { matcher: SimpleMatcher("?"), result: "QuestionMark" },
    { matcher: SimpleMatcher("!"), result: "ExclamationMark" },
    { matcher: StringMatcher(), result: "StringLiteral" },
    { matcher: OrdinalMatcher(), result: "Ordinal" },
    { matcher: IntegralMatcher, result: "Integral" },
];
/// input: String, output: {tokens: [Token], errors: [Error]}
function tokenize(input) {
	var char_index = 0;
	var current = undefined;
	var look_ahead = undefined;
	var tokens = [];
	var errors = [];
	var row = 1;
	var column = 1;
	
	function advance() {
		if (look_ahead) {
			current = look_ahead;
			look_ahead = undefined;
		} else {
			if (current == "\n") {
				row += 1;
				column = 1;
			} else {
				column += 1;
			}
			current = input.charAt(char_index);
			char_index += 1;
		}
	}
	
	function next_token() {
		var matching_token_type = "None";
		var matching_token_string = "";
		var buffer = "";
		
		//reset and shallow copy token_matchers
		var current_matchers = token_matchers.map( (matcher_obj) => { matcher_obj.matcher.reset(); return matcher_obj } );
		
		while (current_matchers.length > 0 && current) {
			buffer += current;
			current_matchers = current_matchers.filter( (matcher_obj) => {
				switch(matcher_obj.matcher.append(current)) {
					case "MatchContinue":
						matching_token_string += buffer;
						matching_token_type = matcher_obj.result;
						buffer = "";
						return true;
					case "Erroneous":
						errors.push(Error("Tokenizing", matcher_obj.matcher.error(), row, column));
					case "MatchStop":
						matching_token_string += buffer;
						matching_token_type = matcher_obj.result;
						buffer = "";
						return false;
					case "Continue":
						return true;
					case "Stop":
						return false;
				}
			});
			if (current_matchers.length === 0 && buffer.length === 2) {
				look_ahead = current;
				current = "" + buffer.charAt(0);
			} else if (current_matchers.length === 0 && buffer.length === 1) {
				current = buffer;
			} else {
				advance()
			}
		}
		if (matching_token_type === "None") {
			errors.push(Error("Tokenizing", "Did not know what to do with string '" + matching_token_string + "'", row, column));
		} else if (matching_token_type === "NewLine") {
			current_matchers.map( (matcher_obj) => {
				if (matcher_obj.matcher.new_line_reset() === "Erroneous") {
					errors.push(Error("Tokenizing", matcher_obj.matcher.error(), row, column));
				}
			})
		}
		return Token(column, matching_token_type, matching_token_string);
	}
	
	advance();
	var debug_maximum = 1000;
	while (current && debug_maximum > 0) {
		debug_maximum -= 1;
		var token = next_token();
		if (token.type !== "Space")
			tokens.push(token);
	}
	
	return {
		tokens: tokens,
		errors: errors,
	}
}
