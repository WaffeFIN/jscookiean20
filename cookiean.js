
function step(f, field, display) {
	return (input) => {
		var obj = f(input);
		var errors = obj.errors;
		if (errors.length > 0)
			alert(errors);
		var output = obj[field]
		if (document.getElementById("print_" + field).checked)
			console.log(Array.isArray(output) ? output.map(display) : display(output));
		return output
	}
}

function test() {
	var steps = [
		step(tokenize, "tokens", display_token),
		step(row_parse, "rows", display_row),
		step(block_parse, "root", display_node),
		step(crumble, "crumbs", display_crumb)
	]
	
	var input = document.getElementById("cookiean").value;
	document.getElementById("output").value = ""
	
	steps.forEach((step) => { input = step(input) })
	run(input)
	
	console.log("Run completed...")
}

