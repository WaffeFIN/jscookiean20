
//RUNNING

function get_scope(name, scopes) {
	for (var scope of scopes) {
		if (Object.keys(scope).includes(name)) {
			return scope
		}
	}
	return scopes[scopes.length - 1]
}

function assign(value, names, scopes) { //TODO think about scopes: scopes are just objects. 'using' simply pushes an object into scope
	var name_index = names.length - 1
	var name = names[name_index]
	var obj = get_scope(name, scopes)
	while (name_index > 0) {
		if (obj[name] === undefined) {
			obj[name] = {}
		}
		obj = obj[name]
		name_index -= 1
		name = names[name_index]
	}
	obj[name] = value
}

function retrieve(name, scopes) {
	if (typeof name !== "string")
		return name
	for (var i = scopes.length - 1; i >= 0; i -= 1) {
		var scope = scopes[i]
		if (Object.keys(scope).includes(name)) {
			return scope[name]
		}
	}
	return undefined
}

function given(dis, dat, scopes, add_crumbs) {
	if (dis === undefined) return retrieve(dat, scopes);
	if (typeof dis === "function") { dis(add_crumbs); return retrieve(dat, scopes) }
	if (typeof dis === "object" && dat !== undefined) {
		var value = dis[dat]
		return typeof value === "function" ? value.bind(null, dis) : value; //TODO bug: this can bind instruction adders too... 
	}
	return dis
}

function evaluate(values, scopes, add_crumbs) {
	var return_value = undefined
	var value;
	do {
		value = values.pop()
		return_value = given(return_value, value, scopes, add_crumbs)
	}
	while (value !== undefined);
	values.push(return_value);
}

function run(crumbs) {
	var scopes = [StandardLibrary]
	var call_stack = []
	var value_stack = []
	var value_list = []
	var index = 0
	
	function add_crumbs(more) {
		crumbs = crumbs.slice(0, index + 1).concat(more).concat(crumbs.slice(index + 1))
	}
	
	while (index < crumbs.length) {
		var crumb = crumbs[index]
		switch(crumb.name) {
			case "Assign":
				assign(value_stack.pop(), crumb.value, scopes)
				break
			case "Push":
				value_stack = value_stack.concat(crumb.value)
				break
			case "Builtin":
				value_stack.push(crumb.value(value_stack.pop()))
				break
			case "Eval":
				evaluate(value_stack, scopes, add_crumbs)
				break
			case "Enter":
				scopes.push({})
				call_stack.push({ value_stack: value_stack, value_list: value_list })
				value_stack = [value_stack.pop()]
				value_list = []
				break
			case "Leave":
				var return_obj = call_stack.pop()
				scopes.pop()
				value_stack = return_obj.value_stack.concat(value_list)
				value_list = return_obj.value_list
				break
			case "Return":
				var return_obj = call_stack.pop()
				var _scope = scopes.pop()
				if (Object.keys(_scope).length > 0) {
					value_stack = return_obj.value_stack.concat([_scope]) //TODO maybe use two different 'Return' crumbs?
				} else {
					value_stack = return_obj.value_stack.concat(value_list) //TODO CookieanList
				}
				value_list = return_obj.value_list
				break
			case "Append":
				value_list.push(value_stack.pop())
				break
			case "Delete":
				delete (scopes[scopes.length - 1])[crumb.value[crumb.value.length - 1]] //TODO fix, might not be necessary after value_list/return object fix
				break
		}
		index += 1
	}
}

